#  This file is used to configure the paramters used to control the servos.
#  This is an example only. To use the controller define a new controller_config.yaml
#  in your ros package along with a new launch file and use those. see the readme for
#  more info.

# # --------------------------------------------------------------------------- # #
# GLOBAL OPERATION PARAMETERS
loop_rate: 100                    # desired rate for joint state updates. actual rate may be less depending on number
                                  # of dynamixels connected and port baud rate.
#control_mode: 'position'            # control mode, either 'position', 'velocity', or 'effort'
control_mode: 'velocity'
#control_mode: 'effort'    ## bug torque ac plusieurs moteurs, même valeur pr chaque moteur
disable_torque_on_shutdown: true  # with this enabled the motors will switch off when the controller closes
ignore_input_velocity: false      # ignore input velocity commands in position mode (no profile velocity)
diagnostics_rate: 1               # rate to publish diagnostic information
dataport_rate: 1                  # rate to read from dynamixel external dataports
recv_queue_size: 10                # receive queue size for desired_joint_states topic
# The below values are used as global defaults and are applied for each servo unless overridden in the entry for
# the servo below
global_max_vel: 6.0               # maximum joint speed (rad/s) (in position or velocity control)
global_torque_limit: 1.0          # maximum motor torque for all modes, given as a fraction of rated max (0-1)
### Used to set all movement in CCW as negative
CCW_negative_convention: 1

## Set structure as only listener (for ex, for haptic device)
## In passive mode (= 1) : effort = load (% of max torque of servomotor)
## In active mode (= 0) : effort = current in servomotor (in A)
## HOWEVER, Goal Torque command will always be a current value (to set as A)
passive_structure: 0
# # --------------------------------------------------------------------------- # #
# PORT AND SERVO CONFIGURATIONS
ports:

  # PORT LIST
  - name: Port_1               # name for this port in config
    device: /dev/ttyUSB0       # serial device this port will communicate on
    baudrate: 2000000          # baudrate in use
    use_legacy_protocol: true # wether to use new 2.0 protocol (false) or legacy 1.0 protocol (true)
    group_read_enabled: true   # specify whether to use group comms for reading
    group_write_enabled: true  # specify whether to use group comms for writing
    servos:
      # SERVO LIST FOR THIS PORT
      - id: 1                  # (ID set in servo eeprom, must be unique on this port)
        joint_name: gimbal_joint1    # (MUST BE UNIQUE ACROSS ALL PORTS)
        #
        # The three values below are mandatory, they define the orientation and zeroing of the dynamixel:
        #
        zero_pos: 2048         # 0 rad servo position (in raw encoder count)
        min_pos: 0             # minimum servo position (in raw encoder count)
        max_pos: 4095          # maximum servo position, Note when MIN > MAX ROTATION IS REVERSED
        #
        # The below arguments are all optional and override the global values:
        #
        max_vel: 6.0           # maximum joint speed (rad/s) (in position or velocity control)
        torque_limit: 1.0      # maximum motor torque for all modes, given as a fraction of rated max (0-1)

        num_turns: 4 ## number of turns possible, starting from one extremity to another. Ex : num_turns = 2 ==> servo can go from [-2*pi; 2*pi]
                      ## mode available only in velocity / effort mode 
                      ## NB : normally, only 2; but it has a really bad tendancy to go higher than 2*pi, thus
                      ## provking a bug
        min_rad_pos: -6.3
        max_rad_pos: 6.3
        sec_coeff: 0.34  ## security coefficient : speed value (rad/s) to apply to get out of articular limits.
                         ## 0.34 rad / s ==> 19.5°/s.

      - id: 2                 # (ID set in servo eeprom, must be unique on this port)
        joint_name: gimbal_joint2    # (MUST BE UNIQUE ACROSS ALL PORTS)
        #
        # The three values below are mandatory, they define the orientation and zeroing of the dynamixel:
        #
        zero_pos: 2048         # 0 rad servo position (in raw encoder count)
        min_pos: 0             # minimum servo position (in raw encoder count)
        max_pos: 4095          # maximum servo position, Note when MIN > MAX ROTATION IS REVERSED
        #
        # The below arguments are all optional and override the global values:
        #
        max_vel: 6.0           # maximum joint speed (rad/s) (in position or velocity control)
        torque_limit: 1.0      # maximum motor torque for all modes, given as a fraction of rated max (0-1)

        num_turns: 2 ## number of turns possible, starting from one extremity to another (from -num_turns*pi ; num_turns*pi). Ex : num_turns = 2 ==> servo can go from [-2*pi; 2*pi]
                      ## mode available only in velocity / effort mode 
                      ## normally, only 1; but again, bad tendancy to create bug               
        min_rad_pos: -3.2
        max_rad_pos: 3.2  
        sec_coeff: 0.34  ## security coefficient : speed value (rad/s) to apply to get out of articular limits.
                         ## 0.34 rad / s ==> 19.5°/s.

      - id: 3               # (ID set in servo eeprom, must be unique on this port)
        joint_name: gimbal_joint3    # (MUST BE UNIQUE ACROSS ALL PORTS)
        #
        # The three values below are mandatory, they define the orientation and zeroing of the dynamixel:
        #
        zero_pos: 2048         # 0 rad servo position (in raw encoder count)
        min_pos: 0             # minimum servo position (in raw encoder count)
        max_pos: 4095          # maximum servo position, Note when MIN > MAX ROTATION IS REVERSED
        #
        # The below arguments are all optional and override the global values:
        #
        max_vel: 6.0           # maximum joint speed (rad/s) (in position or velocity control)
        torque_limit: 1.0      # maximum motor torque for all modes, given as a fraction of rated max (0-1)

        num_turns: 200 ## number of turns possible, starting from one extremity to another. Ex : num_turns = 2 ==> servo can go from [-2*pi; 2*pi]
                      ## mode available only in velocity / effort mode     
                      ## WARNING : CURRENTLY WORKING ONLY FOR PAIR VALUES !!! (e.g -pi --> pi changes occurs at servo = 0)
                
        canResetNumTurns: 1 ## can reset number of turns by service 
                            ## 1 : yes; 0 : no
                            ## ONLY FOR LAST JOINT !!!!
  # more ports can be defined in the same manner as above, each port can even have a different baudrate and protcol
  # - name: Port_2
  #   device: /dev/ttyUSB1
  #   baudrate: 115200
  #   use_legacy_protocol: true
  #   group_read_enabled: true
  #   group_write_enabled: true
  #   servos:
  #     - id: 1               # id only needs to be unique for each port and so id 1 can be reused here
  #       joint_name: joint_3 # name DOES have to be unique though, so we continue the namin_posg scheme
  #       zero_pos: 512
  #       min_pos: 0
  #       max_pos: 1023

  #     - id: 2
  #       joint_name: joint_4
  #       zero_pos: 512
  #       min_pos: 0
  #       max_pos: 1023

# --------------------------------------------------------------------------- # #
